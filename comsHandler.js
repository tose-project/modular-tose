var config = require('config');
var debug = require('debug')('tose:coms');
var info = require('debug')('info:tose:coms');
var events = require('events');
modules = require('./modules');

var coms = [];
var eventEmitters = {};
var listening = true;

var sendMessage = function(channel, cid, message) {
  debug('[send_message] %s to: %s message: %s', channel, cid, message);
  coms[channel].sendMessage(cid, message);
}

module.exports = function (parent) {

  function new_message(service, data) {
    debug('[new_message] %s (%s) from: %s message: %s', service, data.type, data.cid, data.message);
    if (!listening) {
      return
    }
    parent.processMessage(service, data, coms[service].react);
  }
  function inline_search(service, data) {
    debug('[inline_search]', data);
    parent.toseSearch(service, null, data, returnDataInsteadOfSend = true, overrideMaxResults = 10).then(function(torrents) {
      coms[service].answerInlineQuery(data.cid, torrents);
    }).catch(function(err){console.log(err)});
  }
  function web_api(service, data) {
    debug('[web_api] %o', data);
    if (data.feature == 'listen') {
      listening = data.val;
    }
  }

  for (var key in modules) {
    if (modules.hasOwnProperty(key)) {

      if (key.startsWith('com_')) {
        var service = key.substring(4);
        eventEmitters[service] = new events.EventEmitter();
        eventEmitters[service].on('new_message', new_message.bind(undefined, service));
        eventEmitters[service].on('inline_search', inline_search.bind(undefined, service));
        if (service == 'web') {
          eventEmitters[service].on('api', web_api.bind(undefined, service));
        }
        coms[service] = new modules[key](config['coms'][key.substring(4)], eventEmitters[service]);
      }

    }
  }
  return {sendMessage}

}
