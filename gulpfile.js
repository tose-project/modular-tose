var gulp       = require('gulp'),
    eslint     = require('gulp-eslint'),
    uglify     = require('gulp-uglify');
    babel      = require('gulp-babel');


gulp.task('lint', () => {
  return gulp.src(['**/*.js','!node_modules/**'])
      .pipe(eslint())
      .pipe(eslint.format())
      .pipe(eslint.failAfterError());
});

gulp.task('build', function() {
  gulp.src(['./**/*.js', '!node_modules/**'])
  .pipe(babel( {presets: ['es2015']} ))
  .pipe(uglify())
  .pipe(gulp.dest('dist'))
});
