var debug = require('debug')('tose:torrents:torrentapi');
var info = require('debug')('info:tose:torrents:torrentapi');
var axios = require('axios');

function zeroFill( number, width )
{
  width -= number.toString().length;
  if ( width > 0 )
  {
    return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
  }
  return number + ""; // always return a string
}

class Torrentapi {

  constructor(config) {
    this.DOMAIN = 'https://torrentapi.org';
    this.BASE_URL = this.DOMAIN + '/pubapi_v2.php?';
    this.maxResults = config ? config.maxResults : 1;
    this.token = '';
    this.refreshToken();
  }

  refreshToken () {
    debug('Refreshing token.');
    var that = this;
    return axios.get('https://torrentapi.org/pubapi_v2.php?get_token=get_token&app_id=tosebot').then(response => {
      that.token = response.data.token;
    })
    .catch(err => {console.log(err)});
  }

  search (keyword, overrideMaxResults) {
    var newMaxResults = overrideMaxResults ? overrideMaxResults : this.maxResults;
    var that = this;
    var url = this.BASE_URL + 'mode=search&app_id=tosebot&search_string=' + keyword + '&token=' + that.token;
    var torrents = [];

    debug('Searching for: %s with token %s', keyword, that.token);

    return new Promise((resolve, reject) => {
      return axios.get(url).then(response => {
        if (response.data.torrent_results) {
          debug('Found %d torrent(s)', response.data.torrent_results.length);
          response.data.torrent_results.forEach((torrent, index, array) => {
            if (index < Math.min(newMaxResults, array.length-1)) {
              torrents.push({
                name: torrent.filename,
                magnet: torrent.download,
                size: 'N/A'
              });
            }
          });
          resolve(torrents);
        } else if (response.data.error_code == 2) {
          debug('Token expired: %s', that.token);
          return that.refreshToken()
          .then( () => {
            that.search(keyword)
            .then(torrents => {
              resolve(torrents);
            })
          });
        }
      })

    })
    .then(torrents => {
      return torrents
    })
    .catch(err => { log.error(err) })

  }

  episode(series, season, episode) {
    var keyword = series + ' s' + zeroFill(season, 2) + 'e' + zeroFill(episode, 2);
    return this.search(keyword);
  }

}



module.exports = Torrentapi;

// For testing module
// x = new Torrentapi();
// x.search('doctor who s10e02').then(torrents => { console.log(torrents[0]) }).catch(err => {console.log(err)})
// x.refreshToken();
