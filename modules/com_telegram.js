var debug = require('debug')('tose:coms:telegram');
var info = require('debug')('info:tose:coms:telegram');
const TelegramBot = require('node-telegram-bot-api');


class Telegram {

  constructor(config, events) {
    var tempInlineQueries = {};
    this.bot = new TelegramBot(config.authToken, {polling: true});
    this.bot.getMe().then(me => {
      debug(me);
      info('Connected');
    }).catch(err => debug(err));
    this.bot.onText(/(.+)/, (msg, match) => {

      if (match[1].includes('magnet:?xt=')) { return }

      if (msg.chat.type == 'private' || (msg.chat.type == 'group' && match[1].toLowerCase().startsWith('tose '))) {
        if (match[1].toLowerCase().startsWith('tose ')) {
          var message = match[1].substring(5);
        } else {
          var message = match[1];
        }
      } else {
        return
      }

      var data = {cid: msg.chat.id, message: message, type: msg.chat.type == 'group' ? 'group' : 'direct'};
      events.emit('new_message', data);
    });
    this.bot.on('inline_query', function(ev){
      if (!ev.query) { return }
      debug(ev);
      debug('tempInlineQueries: ', tempInlineQueries);
      if (tempInlineQueries[ev.from.username]) {
        clearTimeout(tempInlineQueries[ev.from.username]);
      }
      tempInlineQueries[ev.from.username] = setTimeout(function() {
        debug('Waited, now responding to', ev.from.username)
        events.emit('inline_search', {cid: ev.id, message: ev.query, type: 'direct'});
        delete tempInlineQueries[ev.username];
      }, 3000);
    });
    var that = this;
  }

  sendMessage(cid, message) {
    if (!message) { return }
    this.bot.sendMessage(cid, message);
  }

  answerInlineQuery(queryId, torrents) {
    debug('WEEE GOT TORRENTS:', torrents);
    var results = []
    for (var i = 0; i < torrents.length; i++) {
      results.push({
        type: 'article',
        id: i,
        title: torrents[i].name,
        description: torrents[i].name + ' - ' + torrents[i].size + '\n' + torrents[i].magnet,
        input_message_content: {
          message_text: torrents[i].name + ' - ' + torrents[i].size + '\n' + torrents[i].magnet
        }
      });
    }
    debug('Sending', results.length, 'results.')
    this.bot.answerInlineQuery(queryId, results);
  }

}



module.exports = Telegram;
