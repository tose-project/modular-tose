var debug = require('debug')('tose:torrents:tpb');
var info = require('debug')('info:tose:torrents:tpb');
var cheerio = require('cheerio');
var axios = require('axios');

function zeroFill( number, width )
{
  width -= number.toString().length;
  if ( width > 0 )
  {
    return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
  }
  return number + ""; // always return a string
}

class Tpb {

  constructor(config) {
    this.DOMAIN = 'https://thepiratebay.org';
    this.BASE_URL = this.DOMAIN + '/search/';
    this.maxResults = config ? config.maxResults : 1;
  }

  search (keyword, overrideMaxResults) {
    var newMaxResults = overrideMaxResults ? overrideMaxResults : this.maxResults;
    var that = this;
    var url = this.BASE_URL + keyword + '/0/99/0';
    var torrents = [];

    debug('Searching for: %s', keyword);

    return new Promise( (resolve, reject) => {

      axios.get(url).then(response => {
        var $ = cheerio.load(response.data);
        var results = $('table#searchResult > tr').toArray();
        debug('Found %d torrent(s)', results.length);
        $('table#searchResult tr').each((index, element) => {
          var tr = $(element);
          if (index < Math.min(newMaxResults+1, results.length-1)) {
            torrents.push({
              name: tr.children('td').eq(1).children('div').first().children('a').html(),
              magnet: tr.children('td').eq(1).children('a[title="Download this torrent using magnet"]').attr('href'),//,
              seeds: tr.children('td[align="right"]').first().text(),
              leeches: tr.children('td[align="right"]').last().text(),
              size: tr.children('td').eq(1).children('font').text().split(', ')[1],
              url: that.DOMAIN + tr.children('td').eq(1).children('div').first().children('a').attr('href')
            });
          }
        });
        if (torrents.length >= 1) {
          torrents.shift();
        }
        resolve(torrents);
      })

    })

  }

  episode(series, season, episode) {
    var keyword = series + ' s' + zeroFill(season, 2) + 'e' + zeroFill(episode, 2);
    return this.search(keyword);
  }

}



module.exports = Tpb;

// For testing module
// x = new Tpb();
// x.search('black mirror').then(torrents => { console.log(torrents) }).catch(err => {console.log(err)})
