var debug = require('debug')('tose:web');
var info = require('debug')('info:tose:web');
const path = require('path');
const express = require('express');
const app = express();
var port = process.env.PORT || 80;
var server = require('http').createServer(app);
var io = require('socket.io')(server);

class Web {

  constructor(config, events) {
    app.use('/web', express.static(path.join(__dirname, 'web')))
    app.get('/', function (req, res) {
      res.send('Hello World!');
    });
    app.get('/chat', function (req, res) {
      res.sendFile(__dirname + '/web/chat.html');
    });
    app.get('/core/on', function (req, res) {
      // listening = true;
      events.emit('api', {'feature': 'listen', 'val': true});
      info('Started Listening (API Call)');
      res.json({message: 'listening'});
    });
    app.get('/core/off', function (req, res) {
      // listening = false;
      events.emit('api', {'feature': 'listen', 'val': false});
      info('Stopped listening (API Call)');
      res.json({message: 'not listening'});
    });

    io.on('connection', function(socket){
      // socket.emit('request', /* */); // emit an event to the socket
      // io.emit('broadcast', /* */); // emit an event to all connected sockets
      // socket.on('reply', function(){ /* */ }); // listen to the event
      debug('New client connected.');
    });
    server.listen(port, function () {
      info('Tose listening on port ', port);
    });
  }

}



module.exports = Web;
