var debug = require('debug')('tose:torrents:1337x');
var info = require('debug')('info:tose:torrents:1337x');
var cheerio = require('cheerio');
var axios = require('axios');

function zeroFill( number, width )
{
  width -= number.toString().length;
  if ( width > 0 )
  {
    return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
  }
  return number + ""; // always return a string
}

class x1337 {

  constructor(config) {
    this.DOMAIN = 'https://1337x.to';
    this.BASE_URL = this.DOMAIN + '/search/';
    this.maxResults = config ? config.maxResults : 1;
  }

  search (keyword, overrideMaxResults) {
    var newMaxResults = overrideMaxResults ? overrideMaxResults : this.maxResults;
    var that = this;
    var url = this.BASE_URL + keyword + '/1/';
    var promises = [];
    var torrents = [];

    debug('Searching for: %s', keyword);

    return new Promise( (resolve, reject) => {

      axios.get(url).then(response => {
        var $ = cheerio.load(response.data);
        $('table tr').first().remove();
        $('table tr td.size span').remove();
        var results = $('table tr').toArray();
        debug('Found %d torrent(s)', results.length);
        $('table tr').each((i, element) => {
          var tr = $(element);
          torrents.push({
            name: tr.children('td.name').text(),
            seeds: tr.children('td.seeds').text(),
            leeches: tr.children('td.leeches').text(),
            size: tr.children('td.size').text(),
            url: that.DOMAIN + tr.children('td.name').children('a').last().attr('href')
          });
        });
        debug('Getting magnets');
        torrents.forEach((torrent, index, array) => {
          if (index < Math.min(newMaxResults, array.length-1)) {
            promises.push(
              axios.get(torrent.url).then(response => {
                var magnetPattern = /href="(magnet:\?[\w\d=:&.\-%+]*)"/gi;
                var magnet = magnetPattern.exec(response.data);
                torrents[index]["magnet"] = magnet[1];
                return torrents[index]
              })
            );
          }
        });
      }).then(() => {
        Promise.all(promises).then((allTorrents) => {
          resolve(allTorrents);
        })
      });

    })

  }

  episode(series, season, episode) {
    var keyword = series + ' s' + zeroFill(season, 2) + 'e' + zeroFill(episode, 2);
    return this.search(keyword);
  }

}



module.exports = x1337;

// For testing module
// x = new x1337();
// x.search('doctor who s10e02').then(torrents => { console.log(torrents) }).catch(err => {console.log(err)})
