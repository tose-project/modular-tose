var debug = require('debug')('tose:coms:hangouts');
var info = require('debug')('info:tose:coms:hangouts');
var fs = require('fs');
var Client = require('hangupsjs');
var Q = require('q');

/*
Rithvik:  UgzTQ7JmCpWG_Peinjx4AaABAagB_IuSBQ
*/

class Hangouts {

  constructor (config, events) {
    var convTypes = {};
    if (config.refreshToken || config.cookies) {
      debug('Found cookies and refreshToken in config, using them.');
      
      fs.writeFile("refreshToken.txt", config.refreshToken, function(err) {
        if(err) { return console.error(err) }
        debug('Wrote to refreshToken.txt');
      });
      fs.writeFile("cookies.json", config.cookies, function(err) {
        if(err) { return console.error(err) }
        debug('Wrote to cookies.json');
      });
      
    }
    var configCreds = () => {
      return { auth: () => {return config.authToken} }
    };
    var inputCreds = () => {
      return { auth: Client.authStdin };
    };
    
    // Reverse this when logging in with authToken after a long time
    // this.client = new Client();
    this.client = new Client({rtokenpath: 'refreshToken.txt', cookiespath: 'cookies.json'});
    var that = this;
    if (config.debug) { this.client.loglevel('debug') }

    this.client.on('chat_message', ev => {
      that.client.setpresence(true);
      that.client.setfocus(ev.conversation_id.id);
      if (ev.sender_id.gaia_id != ev.self_event_state.user_id.gaia_id) {

        if (convTypes[ev.conversation_id.id] == 'direct' || convTypes[ev.conversation_id.id] == 'group' && ev.chat_message.message_content.segment[0].text.toLowerCase().startsWith('tose ')) {
          if (ev.chat_message.message_content.segment[0].text.toLowerCase().startsWith('tose ')) {
            var message = ev.chat_message.message_content.segment[0].text.substring(5);
          } else {
            var message = ev.chat_message.message_content.segment[0].text;
          }
        } else {
          return
        }

        var data = {cid: ev.conversation_id.id, type: convTypes[ev.conversation_id.id], message: message};
        events.emit('new_message', data);
      }
      return true
    });

    this.client.on('client_conversation', ev => {
      if (!convTypes[ev.conversation_id.id]) {
        if (ev.type == 'GROUP') {
          convTypes[ev.conversation_id.id] = 'group';
        } else {
          convTypes[ev.conversation_id.id] = 'direct';
        }
      }
      return true
    });

    var react = cid => {
      that.client.settyping(cid, Client.TypingStatus.TYPING);
    }

    var reconnect = () => {
      that.client.connect(configCreds).then(() => {
          info('Connected');
          this.client.syncrecentconversations().then(data => {
            info('Found %d conversation(s)', data.conversation_state.length);
            for (var i = 0; i < data.conversation_state.length; i++) {
              var conv = data.conversation_state[i]
              convTypes[conv.conversation_id.id] = (conv.conversation.type == 'GROUP' ? 'group' : 'direct');
            }
            debug(convTypes);
          });
      });
    };

    this.client.on('connect_failed', () => {
      info('Disconnected');
      Q.Promise(rs => {
          setTimeout(rs,3000);
      }).then(reconnect);
    });

    reconnect();
  }

  sendMessage(cid, message) {
    var bld = new Client.MessageBuilder()
    var segments = bld.text(message).toSegments()
    this.client.sendchatmessage(cid, segments);
  }

}



module.exports = Hangouts;

// For testing module
// x = new Hangouts(() => {})
