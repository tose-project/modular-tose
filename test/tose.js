process.env.NODE_ENV = 'test'

let chai = require('chai');
let should = chai.should();
let assert = require('assert');
let sinon = require('sinon');

var config = require('config');
var tose = require('../index.js');
var torrentHandler = require('../torrentHandler');

describe('Tose core', function() {
  describe('Process messages', function() {

    var testMessage = 'Test message';
    before(function() {
      sinon.stub(tose.comsHandler, 'sendMessage').callsFake(function(channel, cid, message) {
        assert.equal(message, testMessage);
        return message
      });
    });

    it('should echo messages', function() {
      var data = {message: testMessage}
      tose.eventHandler('test', 'new_message', data)
      assert(tose.comsHandler.sendMessage.calledOnce);
    });

    after(function() {
      tose.comsHandler.sendMessage.restore();
    });

  });
});

describe('Torrent modules', function() {

  before(function() {
    sinon.stub(tose.comsHandler)//.callsFake(() => {});
  });

  // Loading modules
  var keyword = "black mirror";
  var services = ['1337x', 'rarbg'];
  services.forEach((service, index, array) => {

    it('Searching for torrents with ' + service, function(done) {
      torrentHandler.search(keyword, service).then(torrents => {
        torrents.should.be.a('array');
        assert.equal(torrents.length, config.maxResults);
        done();
      });
    });

  });

});
