process.env.NODE_ENV = 'test'

let chai = require('chai');
let should = chai.should();
let assert = require('assert');
let sinon = require('sinon');

var config = require('config');
var torrentHandler = require('../torrentHandler');


describe('Torrent modules', function() {

  this.retries(3);

  var keyword = "black mirror";
  var services = Object.keys(torrentHandler.services)
  services.forEach((service, index, array) => {

    it('Searching for torrents with ' + service, function(done) {
      torrentHandler.search(keyword, service).then(torrents => {
        torrents.should.be.a('array');
        assert.equal(torrents.length, config.maxResults);
        done();
      });
    });

  });

});
