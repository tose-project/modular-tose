var cheerio = require('cheerio');
var axios = require('axios');
var fs = require('fs');

var shows = []

axios.get('https://showrss.info/browse').then(response => {
  console.log(response.data);
  var $ = cheerio.load(response.data);
  var options = $('#showselector').find('option');
  $('#showselector').find('option').each( (index, el) => {
    var name = $(el).text().replace(/\s?\(.*\)/g, '');
    shows[index] = {value: name, synonyms: [name]};
  });
}).then(() => {
  shows.splice(0, 1);
  fs.writeFile('shows.json', JSON.stringify(shows), function(err) {
    if(err) {console.log(err)}
    console.log('Wrote to file.');
  });
});
