var debug = require('debug')('tose:torrents');
var info = require('debug')('info:tose:torrents');
var config = require('config');
modules = require('./modules');

var services = [];
for (var key in modules) {
  if (modules.hasOwnProperty(key)) {
    if (key.startsWith('torrent_')) {
      services[key.substring(8)] = new modules[key](config);
    }
  }
}

exports.services = services;

services['rarbg'] = services['torrentapi'];
services['1337x'] = services['x1337'];
services['thepiratebay'] = services['tpb'];
exports.search = (keyword, service=config.defaultTorrentService, overrideMaxResults) => {
  return services[service].search(keyword, overrideMaxResults)
}
exports.episode = (series, season, episode, service=config.defaultTorrentService, overrideMaxResults) => {
  return services[service].episode(series, season, episode)
}
