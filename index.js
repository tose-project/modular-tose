var config = require('config');
if (config.logLevel == 'debug' || config.logLevel == 'trace') {
  // process.env['DEBUG'] = '* -node-telegram-bot-api -express:* -follow-redirects';
  process.env['DEBUG'] = 'tose:*,info:*';
} else {
  // process.env['DEBUG'] = 'info:* -node-telegram-bot-api -express:* -follow-redirects';
  process.env['DEBUG'] = 'info:tose:*';
}
var debug = require('debug')('tose:core');
var info = require('debug')('info:tose:core');
var modules = require('./modules');
var events = require('events');
var apiai = require('apiai');
debug('Loading modules: %o', Object.keys(modules));


var appai = apiai(config.ApiAiToken);


var exports = module.exports = {

  processMessage: function(service, data, react=()=>{}) {
    if (!data.message) { return }
    var testSearch = /search (tpb|rarbg|torrentapi|1337x)?(?: )?(.*)/gi;
    if (testSearch.test(data.message.toLowerCase())) {
      react(data.cid);
      exports.toseSearch(service, type, data);
    } else {
      var request = appai.textRequest(data.message, {
        sessionId: data.cid
      }).on('response', function(response) {
        exports.processAI(service, data, response);
      }).on('error', function(error) {
        console.log(error);
      }).end();
    }
    return
  },

  processAI: function(service, data, response) {
    debug(response.result);
    if (response.result.actionIncomplete) {
      exports.comsHandler.sendMessage(service, data.cid, response.result.fulfillment.speech);
      return
    }
    if (response.result.action == 'get_episode') {
      var testSecode = /s(\d{1,2})(?: )?e(\d{1,2})/gi;
      var regex = testSecode.exec(response.result.resolvedQuery);
      exports.comsHandler.sendMessage(service, data.cid, 'Processed: ' + JSON.stringify(response.result.parameters));
      if ((response.result.parameters.season && response.result.parameters.episode) || (regex && regex[1] && regex[2])) {
        if (regex) {
          var season = regex[1];
          var episode = regex[2];
        } else {
          var season = response.result.parameters.season;
          var episode = response.result.parameters.episode;
        }
        exports.torrentHandler.episode(response.result.parameters.series, season, episode).then(torrents => {
          for (var i = 0; i < torrents.length; i++) {
            exports.comsHandler.sendMessage(service, data.cid, torrents[i].name+' - '+torrents[i].size);
            exports.comsHandler.sendMessage(service, data.cid, torrents[i].magnet);
          }
        });
      } else {
        // Neither (season and episode) nor (secode)
        exports.comsHandler.sendMessage(service, data.cid, 'No info / secode given.');
      }
    } else {
      exports.comsHandler.sendMessage(service, data.cid, response.result.fulfillment.speech);
    }
  },

  toseSearch: function(channel, type, data, returnDataInsteadOfSend = false, overrideMaxResults = 0) {
    return new Promise(function(resolve, reject) {

      var testSearch = /search (tpb|rarbg|torrentapi|1337x)?(?: )?(.*)/gi;
      var regex = testSearch.exec(data.message);
      var service = regex[1] ? regex[1] : 'tpb';
      var keyword = regex[2];
      debug('Searching for %s on %s', keyword, service);
      exports.torrentHandler.search(keyword, service, overrideMaxResults = overrideMaxResults).then(torrents => {
        if (returnDataInsteadOfSend) {
          resolve(torrents)
        } else {
          for (var i = 0; i < torrents.length; i++) {
            exports.comsHandler.sendMessage(channel, data.cid, torrents[i].name+' - '+torrents[i].size);
            exports.comsHandler.sendMessage(channel, data.cid, torrents[i].magnet);
          }
          resolve()
        }
      });

    })
  }

}

// var torrentHandler = require('./torrentHandler');
// var comsHandler = require('./comsHandler')(exports);
if (process.env.NODE_ENV != 'test') {
  exports['torrentHandler'] = require('./torrentHandler');
  exports['comsHandler'] = require('./comsHandler')(exports);
} else {
  exports['torrentHandler'] = {};
  exports['comsHandler'] = {
    sendMessage: function(message) {
      return message
    }
  };
}
